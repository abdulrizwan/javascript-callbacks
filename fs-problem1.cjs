/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 

    Ensure that the function is invoked as follows: 
        fsProblem1(absolutePathOfRandomDirectory, randomtotalFiles)
*/

const fs = require('fs')

const path = require('path')


let createFile = (directoryPath, totalFiles, callback) => {
    let pathArray = [];
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            return callback(err)
        } else {
            let createdFiles = 0;
            for (let index = 0; index < totalFiles && createdFiles < totalFiles; index++) {
                let fileName = `file${index}.json`
                const filePath = path.join(directoryPath, fileName)
                pathArray.push(filePath)
                let generateData = () => {
                    let data = {
                        someData: Math.random()
                    }
                    return JSON.stringify(data)
                }
                let fileData = generateData()
                fs.writeFile(filePath, fileData, (err) => {
                    if (err) {
                        return callback(err)
                    }
                    else {
                        createdFiles++
                        if (createdFiles === totalFiles) {
                            return callback(null, directoryPath, pathArray)
                        }
                    }
                })
            }
        }
    })

}



let deleteFile = (totalFiles, pathArray, callback) => {
    for (let index = 0; index < totalFiles; index++) {
        let fileName = pathArray[index]
        fs.unlink(fileName, (err) => {
            if (err) {
                return callback(err)
            }
            else if (index === pathArray.length - 1) {
                return callback(null)
            }
        })
    }
}

let fsProblem1 = (directoryPath, totalFiles) => {
    createFile(directoryPath, totalFiles, (err, directoryPath, pathArray) => {
        if (err) {
            console.log('error', err)
        }
        else {
            console.log('created requried json files ', directoryPath)
            deleteFile(totalFiles, pathArray, (err) => {
                if (err) {
                    console.log("Error ", err)
                }
                else {
                    console.log('deleted all files in ', directoryPath)
                }
            })
        }

    })
}

module.exports = fsProblem1







