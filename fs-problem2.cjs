let cumulativeFuntion = () => {

    const fs = require('fs')
  
    let  readFileFunction = (filename, callback) => {
      fs.readFile(filename, 'utf8', (err, data) => {
        if (err) {
          callback(err)
        } else {
          callback(null, data)
        }
      })
    }
  
    let  writeFileFunction = (filename, data, callback) => {
      fs.writeFile(filename, data, 'utf8', (err) => {
        if (err) {
          callback(err)
        } else {
          callback(null)
        }
      })
    }
  
    let  convertToUpperCase = (content) =>{
      return content.toUpperCase()
    }
  
    let  convertToLowerCaseAndSplit = (content) => {
      const lowercaseContent = content.toLowerCase()
      return lowercaseContent.split(/[.!?]\s+/)
    }
  
    
    const fileName = 'lipsum.txt'
  
    readFileFunction(fileName, (err, data) => {
      if (err) {
        console.error('Error :', err)
      } else {
        const uppercaseContent = convertToUpperCase(data)
        const newUppercaseFileName = 'uppercase.txt'
        writeFileFunction(newUppercaseFileName, uppercaseContent, (err) => {
          if (err) {
            console.error('Error :', err)
          } else {
            console.log('Uppercase content being written to', newUppercaseFileName)
  
            const lowercaseAndSplitContent = convertToLowerCaseAndSplit(uppercaseContent)
            const newLowercaseSplitFileName = 'lowercase_split.txt'
            writeFileFunction(newLowercaseSplitFileName, lowercaseAndSplitContent.join('\n'), (err) => {
              if (err) {
                console.error('Error :', err)
              } else {
                console.log('Lowercase split content being written to', newLowercaseSplitFileName)
  
                const filenames = [newUppercaseFileName, newLowercaseSplitFileName]
                const sortedContent = filenames
                  .map((filename) => fs.readFileSync(filename, 'utf8'))
                  .sort()
                  .join('\n')
                const newSortedFileName = 'sorted.txt'
                writeFileFunction(newSortedFileName, sortedContent, (err) => {
                  if (err) {
                    console.error('Error :', err)
                  } else {
                    console.log('Sorted content written to', newSortedFileName)
  
                    const filenamesContent = filenames.join('\n')
                    const filenamesFileName = 'filenames.txt'
                    writeFileFunction(filenamesFileName, filenamesContent, (err) => {
                      if (err) {
                        console.error('Error :', err)
                      } else {
                        console.log('Filenames written to', filenamesFileName)
  
                        readFileFunction(filenamesFileName, (err, data) => {
                          if (err) {
                            console.error('Error :', err)
                          } else {
                            const filesToDelete = data.split('\n').filter(Boolean)
                            filesToDelete.forEach((file) => {
                              fs.unlink(file, (err) => {
                                if (err) {
                                  console.error('Error :', err)
                                } else {
                                  console.log('File deleted:', file)
                                }
                              })
                            })
                          }
                        })
                      }
                    })
                  }
                })
              }
            })
          }
        })
      }
    })
  
  }
  
module.exports = cumulativeFuntion