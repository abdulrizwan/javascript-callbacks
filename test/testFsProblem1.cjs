const fsproblem1 = require('../fs-problem1.cjs')
const path = require('path')

let pathOfDirectory = path.join(__dirname, 'sample')
let randomNumberOfFiles = 5;
fsproblem1(pathOfDirectory, randomNumberOfFiles);
